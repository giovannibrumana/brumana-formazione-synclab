public class Immagine extends ElementoMultimediale implements ILuminosità {
    private int luminosità;

    public Immagine(String titolo) {
        super(titolo);

        luminosità = 5;
    }

    public void show() {
        String print = super.getTitolo();

        for (int i = 0; i < luminosità; i++) {
            print += "*";
        }

        System.out.println(print);
    }

    public void darker() {
        if (luminosità > 0) {
            luminosità -= 1;
        }
    }

    public void brighter() {
        luminosità += 1;
    }
}