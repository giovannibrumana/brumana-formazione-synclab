import java.lang.reflect.MalformedParametersException;

public abstract class ElementoMultimedialeRiproducibile extends ElementoMultimediale {
    private int durata;

    public ElementoMultimedialeRiproducibile(String titolo, int durata) {
        super(titolo);
        if (durata < 0) throw new MalformedParametersException();
        this.durata = durata;
    }

    public int getDurata() {
        return durata;
    }

    public abstract void play();
}
