import java.util.Scanner;

public class Lettore {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        ElementoMultimediale[] array = new ElementoMultimediale[5];

        for (int i = 0; i < array.length; i++) {
            boolean isValid = false;
            char choice = '0';

            do {
                System.out.println("Vuoi inserire un audio (A), un'immagine (I) o un filmato (F)?");
                String answer = s.nextLine();
                if (answer.equalsIgnoreCase("a") || answer.equalsIgnoreCase("i") || answer.equalsIgnoreCase("f")) {
                    choice = answer.toLowerCase().charAt(0);
                    isValid = true;
                } else {
                    System.out.println("Scelta invalida.");
                    isValid = false;
                }
            } while (isValid == false);

            String titolo = "";
            do {
                System.out.println("Inserisci un titolo:");
                titolo = s.nextLine();
                if (titolo.isBlank() == false) {
                    isValid = true;
                } else {
                    System.out.println("Scelta invalida.");
                    isValid = false;
                }
            } while (isValid == false);

            int durata = 0;
            switch (choice) {
            case 'a':
                do {
                    System.out.println("Inserisci una durata:");
                    String answer = s.nextLine();
                    try {
                        durata = Integer.parseInt(answer);
                        isValid = true;
                    } catch (NumberFormatException e) {
                        System.out.println("Scelta invalida.");
                        isValid = false;
                    }
                } while (isValid == false);
                array[i] = new Audio(titolo, durata);
                break;
            case 'i':
                array[i] = new Immagine(titolo);
                break;
            case 'f':
                do {
                    System.out.println("Inserisci una durata:");
                    String answer = s.nextLine();
                    try {
                        durata = Integer.parseInt(answer);
                        isValid = true;
                    } catch (NumberFormatException e) {
                        System.out.println("Scelta invalida.");
                        isValid = false;
                    }
                } while (isValid == false);
                array[i] = new Filmato(titolo, durata);
                break;
            }
        }

        String azione = "";
        while (azione.equals("0") == false) {
            System.out.println("Quale elemento vuoi eseguire? (Da 1 a 5 per selezionare l'elemento, 0 per terminare)");
            azione = s.nextLine();
            if ((azione.equals("0") ||
                azione.equals("1") ||
                azione.equals("2") ||
                azione.equals("3") ||
                azione.equals("4") ||
                azione.equals("5")) == false) {
                    continue;
                }
            if (azione.equals("0")) break;
            int index = Integer.parseInt(azione) - 1;

            ElementoMultimediale e = array[index];

            if (e instanceof Audio) {
                Audio audio = (Audio) e;
                audio.play();
                System.out.println("Vuoi modificare il volume? (1 aumenta, 0 lascia invariato, -1 diminuisci)");
                String v = s.nextLine();
                if (v.equals("1")) {
                    audio.louder();
                } else if (v.equals("-1")) {
                    audio.weaker();
                }
            } else if (e instanceof Filmato) {
                Filmato filmato = (Filmato) e;
                filmato.play();
                System.out.println("Vuoi modificare il volume? (1 aumenta, 0 lascia invariato, -1 diminuisci)");
                String v = s.nextLine();
                if (v.equals("1")) {
                    filmato.louder();
                } else if (v.equals("-1")) {
                    filmato.weaker();
                }
                System.out.println("Vuoi modificare la luminosità? (1 aumenta, 0 lascia invariato, -1 diminuisci)");
                String l = s.nextLine();
                if (l.equals("1")) {
                    filmato.brighter();
                } else if (l.equals("-1")) {
                    filmato.darker();
                }
            } else if (e instanceof Immagine) {
                Immagine immagine = (Immagine) e;
                immagine.show();
                System.out.println("Vuoi modificare la luminosità? (1 aumenta, 0 lascia invariato, -1 diminuisci)");
                String l = s.nextLine();
                if (l.equals("1")) {
                    immagine.brighter();
                } else if (l.equals("-1")) {
                    immagine.darker();
                }
            }
        }

        s.close();
    }
}