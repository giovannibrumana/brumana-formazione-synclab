import java.lang.reflect.MalformedParametersException;

public abstract class ElementoMultimediale {
    private String titolo;

    public ElementoMultimediale(String titolo) {
        if (titolo == null || titolo.isBlank()) throw new MalformedParametersException();
        this.titolo = titolo;
    }

    public String getTitolo() {
        return titolo;
    }
}
