public class Filmato extends ElementoMultimedialeRiproducibile implements IVolume, ILuminosità {
    private int volume;
    private int luminosità;

    public Filmato(String titolo, int durata) {
        super(titolo, durata);
        volume = 0;
        luminosità = 5;
    }

    public void play() {
        String print = super.getTitolo();
        
        for (int i = 0; i < volume; i++) {
            print += "!";
        }

        for (int i = 0; i < luminosità; i++) {
            print += "*";
        }

        for (int i = 0; i < super.getDurata(); i++) {
            System.out.println(print);
        }
    }

    public void weaker() {
        if (volume > 0) {
            volume -= 1;
        }
    }

    public void louder() {
        volume += 1;
    }

    public void darker() {
        if (luminosità > 0) {
            luminosità -= 1;
        }
    }

    public void brighter() {
        luminosità += 1;
    }
}
