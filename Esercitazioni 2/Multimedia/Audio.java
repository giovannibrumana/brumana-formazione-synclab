public class Audio extends ElementoMultimedialeRiproducibile implements IVolume {
    private int volume;

    public Audio(String titolo, int durata) {
        super(titolo, durata);
        volume = 0;
    }

    public void play() {
        String print = super.getTitolo();
        
        for (int i = 0; i < volume; i++) {
            print += "!";
        }

        for (int i = 0; i < super.getDurata(); i++) {
            System.out.println(print);
        }
    }

    public void weaker() {
        if (volume > 0) {
            volume -= 1;
        }
    }

    public void louder() {
        volume += 1;
    }
}
