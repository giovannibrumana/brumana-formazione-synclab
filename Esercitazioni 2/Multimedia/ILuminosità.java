public interface ILuminosità {
    public void darker();
    public void brighter();
}
