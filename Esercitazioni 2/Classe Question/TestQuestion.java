import java.util.Random;

public class TestQuestion {
    public static void main(String[] args) {
        QuestionYesNo q1 = new QuestionYesNo("È vero che la Terra è piatta? (Rispondere con sì o no)", YesNo.No, 1);
        QuestionYesNo q2 = new QuestionYesNo("È vero che la corrente elettrica si può ottenere tramite fonti rinnovabili, come il Sole? (Rispondere con sì o no)",
             YesNo.Sì, 1);

        NumericQuestion q3 = new NumericQuestion("Quanto vale la radice quadrata di 64?", 8, 2);
        NumericQuestion q4 = new NumericQuestion("In che anno fu scoperta l'America?", 1492, 2);

        MultipleQuestion q5 = new MultipleQuestion("Quale di queste affermazioni è esatta? (Rispondere indicando il numero di risposta corretto)",
            2, new String[] {
                "Le batterie più diffuse al giorno d'oggi funzionano a nichel-cadmio",
                "John Nash ha ricevuto il premio Nobel per l'economia nel 1994",
                "Neil Armstrong fu il primo uomo ad andare nello spazio",
                "Il linguaggio C è stato il primo linguaggio ad avere un approccio orientato agli oggetti"
            }, 5);
        MultipleQuestion q6 = new MultipleQuestion("Quale tra le seguenti attrici ha ricevuto più premi Oscar? (Rispondere indicando il numero di risposta corretto)",
            1, new String[] {
                "Meryl Streep",
                "Julia Roberts",
                "Julianne Moore",
                "Helen Mirren"
            }, 3);

        Question[] domande = new Question[] {q1, q2, q3, q4, q5, q6};

        Random r = new Random();

        Question[] interrogazione = new Question[3];
        int index = 0;
        do {
            int i = r.nextInt(6);
            boolean isValid = true;
            for (int j = 0; j < index; j++) {
                if (interrogazione[j] == domande[i]) {
                    isValid = false;
                }
            }

            if (isValid == true) {
                interrogazione[index] = domande[i];
                index += 1;
            }
        } while (index < interrogazione.length);

        int punteggio = 0;
        for (int i = 0; i < interrogazione.length; i++) {
            punteggio += interrogazione[i].ask();
            System.out.println();
        }

        System.out.println("Hai totalizzato " + punteggio + " punti.");
    }
}