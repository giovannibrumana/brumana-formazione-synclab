import java.util.Scanner;

public class Question {
    private String domanda;
    private String risposta;
    private int punteggio;

    public Question(String domanda, String risposta, int punteggio) {
        this.domanda = domanda;
        this.risposta = risposta;
        this.punteggio = punteggio;
    }

    public int ask() {
        Scanner s = new Scanner(System.in);
        System.out.println(domanda);
        String r = s.nextLine();
        s.close();

        if (r.equals(risposta)) {
            System.out.println("Corretto.");
            return punteggio;
        } else {
            System.out.println("Errato.");
            return 0;
        }
    }

    protected String getDomanda() {
        return domanda;
    }

    protected String getRisposta() {
        return risposta;
    }

    protected int getPunteggio() {
        return punteggio;
    }
}