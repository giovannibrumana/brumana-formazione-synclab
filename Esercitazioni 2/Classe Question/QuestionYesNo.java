import java.util.Scanner;

public class QuestionYesNo extends Question {
    public QuestionYesNo(String domanda, YesNo risposta, int punteggio) {
        super(domanda, risposta.name(), punteggio);
    }

    @Override
    public int ask() {
        Scanner s = new Scanner(System.in);
        String r = "";
        do {
            System.out.println(super.getDomanda());
            r = s.nextLine();
        } while ((r.equalsIgnoreCase(YesNo.Sì.name()) || r.equalsIgnoreCase(YesNo.No.name()) || r.equalsIgnoreCase("si")) == false);

        if (super.getRisposta().equals(YesNo.Sì.name()) && r.equalsIgnoreCase("si") || r.equalsIgnoreCase(super.getRisposta())) {
            System.out.println("Corretto.");
            return super.getPunteggio();
        } else {
            System.out.println("Errato.");
            return 0;
        }
    }
}