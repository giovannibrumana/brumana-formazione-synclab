import java.util.Arrays;
import java.util.Scanner;

public class MultipleQuestion extends NumericQuestion {
    private String[] opzioni;

    public MultipleQuestion(String domanda, int risposta, String[] opzioni, int punteggio) {
        super(domanda, risposta, punteggio);
        this.opzioni = Arrays.copyOf(opzioni, opzioni.length);
    }

    @Override
    public int ask() {
        Scanner s = new Scanner(System.in);
        boolean isValid = false;
        int answer = 0;
        do {
            System.out.println(super.getDomanda());

            for (String opzione : opzioni) {
                System.out.println(opzione);
            }

            String r = s.nextLine();
            try {
                answer = Integer.parseInt(r);
                if (answer > 0 && answer <= opzioni.length) {
                    isValid = true;
                } else {
                    isValid = false;
                }
            } catch (NumberFormatException e) {
                isValid = false;
            }
        } while (isValid == false);

        if (super.getRisposta().equals(String.valueOf(answer))) {
            System.out.println("Corretto.");
            return super.getPunteggio();
        } else {
            System.out.println("Errato.");
            return 0;
        }
    }
}