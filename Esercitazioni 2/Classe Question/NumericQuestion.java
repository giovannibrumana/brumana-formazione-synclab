import java.util.Scanner;

public class NumericQuestion extends Question {
    public NumericQuestion(String domanda, int risposta, int punteggio) {
        super(domanda, String.valueOf(risposta), punteggio);
    }

    @Override
    public int ask() {
        Scanner s = new Scanner(System.in);
        boolean isValid = false;
        int answer = 0;
        do {
            System.out.println(super.getDomanda());
            String r = s.nextLine();
            try {
                answer = Integer.parseInt(r);
                isValid = true;
            } catch (NumberFormatException e) {
                isValid = false;
            }
        } while (isValid == false);

        if (super.getRisposta().equals(String.valueOf(answer))) {
            System.out.println("Corretto.");
            return super.getPunteggio();
        } else {
            System.out.println("Errato.");
            return 0;
        }
    }
}