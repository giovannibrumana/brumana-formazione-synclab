public class Garage {
    public double repair(Vehicle v) {
        if (v == null) return 0;

        if (v.getGuasto() == false) return 20;

        v.setGuasto(false);

        if (v instanceof Car) {
            return 500;
        }
        if (v instanceof Motocycle) {
            return 200;
        }

        throw new UnknownVehicleException();
    }
 
}

class UnknownVehicleException extends RuntimeException {
    
}