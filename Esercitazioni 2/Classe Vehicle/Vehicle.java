public abstract class Vehicle {
    private String targa;
    private String marca;
    private String modello;

    private boolean guasto;

    public Vehicle(String targa, String marca, String modello) {
        this.targa = targa;
        this.marca = marca;
        this.modello = modello;

        guasto = false;
    }

    public String getTarga() {
        return targa;
    }

    public String getMarca() {
        return marca;
    }

    public String getModello() {
        return modello;
    }

    public boolean getGuasto() {
        return guasto;
    }

    public void setGuasto(boolean guasto) {
        this.guasto = guasto;
    }
}
