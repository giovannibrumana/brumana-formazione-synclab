public class Car extends Vehicle {
    private String tipologia;

    public Car(String targa, String marca, String modello, String tipologia) {
        super(targa, marca, modello);
        this.tipologia = tipologia;
    }

    public String getTipologia() {
        return tipologia;
    }
}