public class Motocycle extends Vehicle {
    private int cilindrata;

    public Motocycle(String targa, String marca, String modello, int cilindrata) {
        super(targa, marca, modello);
        this.cilindrata = cilindrata;
    }

    public int getCilindrata() {
        return cilindrata;
    }
}