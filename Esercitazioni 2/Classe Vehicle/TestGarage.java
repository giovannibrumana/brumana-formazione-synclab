public class TestGarage {
    public static void main(String[] args) {
        Car c1 = new Car("EX123FF", "Tesla", "Model X", "SUV");
        Car c2 = new Car("DT720VK", "BMW", "Serie 4", "Utilitaria");
        Car c3 = new Car("FU294BW", "Range Rover", "Evoque", "SUV");

        Motocycle m1 = new Motocycle("X6PX3H", "Yamaha", "Aerox R", 50);
        Motocycle m2 = new Motocycle("GB392XH", "Suzuki", "Alfa", 400);

        Vehicle[] array = new Vehicle[] {c1, c2, c3, m1, m2};

        c2.setGuasto(true);
        m1.setGuasto(true);
        m2.setGuasto(true);

        Garage g = new Garage();

        for (int i = 0; i < array.length; i++) {
            double price = g.repair(array[i]);
            System.out.println("Vehicle: " + array[i].getTarga() + ", price: " + price);
        }
    }
}