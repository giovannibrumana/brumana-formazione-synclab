import java.util.ArrayList;

public class Cartoleria {
    private ArrayList<Cancelleria> magazzino;
    private ArrayList<Ordine> ordini;

    public Cartoleria() {
        magazzino = new ArrayList<>();
        ordini = new ArrayList<>();
    }

    public void aggiungiArticolo(Cancelleria articolo) {
        if (articolo == null) return;
        magazzino.add(articolo);
    }
    
    public void stampaGiacenze() {
        int gomme = 0;
        int penne = 0;

        for (Cancelleria articolo : magazzino) {
            if (articolo instanceof Gomma) {
                gomme += 1;
            } else if (articolo instanceof Penna) {
                penne += 1;
            }
        }

        System.out.println("Numero gomme: " + gomme + ", numero penne: " + penne + ".");
    }

    public void stampaValoreGiacenze() {
        double valore = 0;

        for (Cancelleria articolo : magazzino) {
            valore += articolo.getPrezzoVendita();
        }

        System.out.println("Valore giacenze: " + valore + " euro.");
    }

    public void aggiungiOrdine(int numero, Cliente cliente) {
        if (cliente == null) return;
        if (getOrdine(numero) == null) {
            ordini.add(new Ordine(numero, cliente));
            System.out.println("Ordine aggiunto.");
        } else {
            System.out.println("Numero di ordine già esistente.");
        }
    }

    public Ordine getOrdine(int numero) {
        for (Ordine o : ordini) {
            if (o.getNumero() == numero) {
                return o;
            }
        }

        return null;
    }

    public void aggiungiMerce(int numero, Cancelleria articolo) {
        if (articolo == null) return;
        Ordine o = getOrdine(numero);
        if (o == null) return;

        o.aggiungiMerce(articolo);
    }

    public boolean evadiOrdine(int numero) {
        Ordine o = getOrdine(numero);
        
        for (Cancelleria articolo : o.getMerci()) {
            if (!magazzino.contains(articolo)) {
                System.out.println("Uno o più articoli non presenti in magazzino.");
                return false;
            }
        }

        if (o.chiudiOrdine()) {
            for (Cancelleria articolo : o.getMerci()) {
                magazzino.remove(articolo);
            }
            System.out.println("Ordine evaso.");
            return true;
        } else {
            System.out.println("Denaro insufficiente.");
            return false;
        }
    }
}
