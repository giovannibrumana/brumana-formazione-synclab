public class Penna extends Cancelleria {
    private ColorePenna colore;

    public Penna(String marca, String modello, double costo, ColorePenna colore) {
        super(marca, modello, costo);
        this.colore = colore;
    }

    public ColorePenna getColore() {
        return colore;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Penna) {
            Penna articolo = (Penna) o;
            if (articolo.getMarca().equals(super.getMarca()) && 
                articolo.getModello().equals(super.getModello()) && 
                articolo.getCosto() == super.getCosto() && 
                articolo.getColore().equals(this.colore)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
