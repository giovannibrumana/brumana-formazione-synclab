public class Test {
    public static void main(String[] args) {
        Cartoleria cartoleria = new Cartoleria();
        cartoleria.aggiungiArticolo(new Penna("a", "a", 1, ColorePenna.ROSSO));
        cartoleria.aggiungiArticolo(new Penna("b", "b", 10, ColorePenna.NERO));
        cartoleria.aggiungiArticolo(new Penna("c", "c", 120, ColorePenna.BLU));
        cartoleria.aggiungiArticolo(new Gomma("d", "d", 5, 10, FormaGomma.TRIANGOLARE));
        cartoleria.aggiungiArticolo(new Gomma("e", "e", 3, 8, FormaGomma.ROTONDA));
        cartoleria.aggiungiArticolo(new Gomma("f", "f", 2, 6, FormaGomma.RETTANGOLARE));

        Privato p1 = new Privato(100);
        Azienda a1 = new Azienda(1000);

        cartoleria.aggiungiOrdine(1, p1);
        cartoleria.aggiungiOrdine(2, a1);
        cartoleria.aggiungiMerce(1, new Penna("a", "a", 1, ColorePenna.ROSSO));
        cartoleria.aggiungiMerce(2, new Penna("a", "a", 1, ColorePenna.ROSSO));
        cartoleria.evadiOrdine(1);
        cartoleria.evadiOrdine(2);

        cartoleria.aggiungiOrdine(3, p1);
        cartoleria.aggiungiMerce(3, new Gomma("d", "d", 5, 10, FormaGomma.TRIANGOLARE));
        cartoleria.aggiungiMerce(3, new Penna("c", "c", 120, ColorePenna.BLU));
        cartoleria.evadiOrdine(3);
    }
}