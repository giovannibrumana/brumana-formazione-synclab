public class Privato extends Cliente {
    private double contanti;

    public Privato(double liquidità) {
        super();
        contanti = liquidità;
    }

    public boolean paga(double prezzo) {
        if (prezzo <= contanti) {
            contanti -= prezzo;
            return true;
        } else {
            return false;
        }
    }
}