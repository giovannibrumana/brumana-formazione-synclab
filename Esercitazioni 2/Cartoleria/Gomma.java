public class Gomma extends Cancelleria {
    private double dimensione;
    private FormaGomma forma;

    public Gomma(String marca, String modello, double costo, double dimensione, FormaGomma forma) {
        super(marca, modello, costo);
        this.dimensione = dimensione;
        this.forma = forma;
    }

    public double getDimensione() {
        return dimensione;
    }

    public FormaGomma getForma() {
        return forma;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Gomma) {
            Gomma articolo = (Gomma) o;
            if (articolo.getMarca().equals(super.getMarca()) && 
                articolo.getModello().equals(super.getModello()) && 
                articolo.getCosto() == super.getCosto() && 
                articolo.getDimensione() == this.dimensione &&
                articolo.getForma().equals(this.forma)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}