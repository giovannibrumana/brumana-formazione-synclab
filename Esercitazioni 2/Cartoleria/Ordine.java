import java.util.ArrayList;
import java.util.Date;

public class Ordine {
    private Date data;
    private int numero;
    private Cliente cliente;
    private ArrayList<Cancelleria> merci;

    public Ordine(int numero, Cliente cliente) {
        this.data = new Date();
        this.numero = numero;
        this.cliente = cliente;
        merci = new ArrayList<>();
    }

    protected void aggiungiMerce(Cancelleria articolo) {
        if (articolo == null) return;
        merci.add(articolo);
    }

    public ArrayList<Cancelleria> getMerci() {
        return merci;
    }

    public int getNumero() {
        return numero;
    }

    public double calcolaTotale() {
        double totale = 0;

        for (Cancelleria articolo : merci) {
            totale += articolo.getPrezzoVendita();
        }

        return totale;
    }

    public boolean chiudiOrdine() {
        double totale = calcolaTotale();

        return cliente.paga(totale);
    }
}