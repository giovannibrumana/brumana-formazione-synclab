public abstract class Cancelleria {
    private String marca;
    private String modello;
    private double costo;

    public Cancelleria(String marca, String modello, double costo) {
        this.marca = marca;
        this.modello = modello;
        this.costo = costo;
    }

    public String getMarca() {
        return marca;
    }

    public String getModello() {
        return modello;
    }

    public double getCosto() {
        return costo;
    }

    public double getPrezzoVendita() {
        return 2 * costo;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Cancelleria) {
            Cancelleria articolo = (Cancelleria) o;
            if (articolo.getMarca().equals(this.marca) && articolo.getModello().equals(this.modello) && articolo.getCosto() == this.costo) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}