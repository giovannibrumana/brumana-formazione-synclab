public class Azienda extends Cliente {
    private BancaAzienda contoCorrente;

    public Azienda(double liquidità) {
        contoCorrente = new BancaAzienda(liquidità);
    }

    public boolean paga(double prezzo) {
        return contoCorrente.richiediPagamento(prezzo);
    }
}

class BancaAzienda {
    private double liquidità;

    public BancaAzienda(double liquidità) {
        this.liquidità = liquidità;
    }

    public boolean richiediPagamento(double prezzo) {
        if (prezzo <= liquidità) {
            liquidità -= prezzo;
            return true;
        } else {
            return false;
        }
    }
}