public class Employee extends Person {
    private int annoAssunzione;
    private double stipendio;

    public Employee(String surname, String name, String tax_code, String city, int annoAssunzione, double stipendio) {
        super(surname, name, tax_code, city);
        this.annoAssunzione = annoAssunzione;
        this.stipendio = stipendio;
    }

    public int getAnnoAssunzione() {
        return annoAssunzione;
    }

    public double getStipendio() {
        return stipendio;
    }

    public void setAnnoAssunzione(int annoAssunzione) {
        this.annoAssunzione = annoAssunzione;
    }

    public void setStipendio(double stipendio) {
        this.stipendio = stipendio;
    }

    public void visualize() {
        System.out.println(this.toString());
    }

    public boolean gainsMore(Employee e) {
        return stipendio > e.getStipendio();
    }

    public String toString() {
        return super.toString() + ", anno di assunzione: " + annoAssunzione + ", stipendio: " + stipendio + " euro.";
    }
}
