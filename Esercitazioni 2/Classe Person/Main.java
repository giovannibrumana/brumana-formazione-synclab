public class Main {
    public static void main(String[] args) {
        Person p = new Person("Brumana", "Giovanni", "BRMGNN99M01E507A", "Dolzago");
        System.out.println("Cognome: " + p.getSurname() + ", nome: " + p.getName() + ", anno di nascita: " + p.bornYear() + ".");

        Stagista s1 = new Stagista("Brumana", "Giovanni", "BRMGNN99M01E507A", "Dolzago", 10, 845583);
        Stagista s2 = new Stagista("Rossi", "Mario", "RSSMRA92A14E507X", "Lecco", 20, 123456);
        Stagista s3 = new Stagista("Chen", "Wang", "CHNWNG01P24E507B", "Milano", 30, 654321);

        Stagista[] array = new Stagista[3];
        array[0] = s1;
        array[1] = s2;
        array[2] = s3;

        System.out.println(youngest(array).toString());
    }

    static Stagista youngest(Stagista[] array) {
        if (array == null || array.length == 0) return null;
        Stagista y = array[0];
        for (int i = 1; i < array.length; i++) {
            if (y.bornYear() < array[i].bornYear()) {
                y = array[i];
            }
        }

        return y;
    }
}