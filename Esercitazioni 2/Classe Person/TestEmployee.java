public class TestEmployee {
    public static void main(String[] args) {
        Employee e1 = new Employee("Rossi", "Mario", "RSSMRA64E21P507A", "Milano", 2004, 2000);
        Employee e2 = new Employee("Verdi", "Lucia", "VRDLCU82P68N285F", "Roma", 1998, 2500);
        Employee e3 = new Employee("Bianchi", "Rosa", "BCHRSO99M01E507A", "Firenze", 2020, 1300);

        Employee[] array = new Employee[] {e1, e2, e3};

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i].gainsMore(array[j]) == false) { //Ordino dal più stipendiato al meno stipendiato
                    Employee tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        }

        for (int i = 0; i < array.length; i++) {
            array[i].visualize();
        }
    }
}