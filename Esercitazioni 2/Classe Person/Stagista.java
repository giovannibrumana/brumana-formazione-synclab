public class Stagista extends Person {
    private int numberOfPresence;
    private int idNumber;

    public Stagista(String surname, String name, String tax_code, String city, int numberOfPresence, int idNumber) {
        super(surname, name, tax_code, city);
        this.numberOfPresence = numberOfPresence;
        this.idNumber = idNumber;
    }

    public int getNumberOfPresence() {
        return numberOfPresence;
    }

    public int getIdNumber() {
        return idNumber;
    }

    public void setNumberOfPresence(int numberOfPresence) {
        this.numberOfPresence = numberOfPresence;
    }

    public void setIdNumber(int idNumber) {
        this.idNumber = idNumber;
    }

    @Override
    public String toString() {
        return super.toString() + ", number of presences: " + numberOfPresence + ", id number: " + idNumber;
    }
}