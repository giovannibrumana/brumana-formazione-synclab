import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Person {
    private String surname;
    private String name;
    private String tax_code;
    private String city;

    public Person() {
        surname = null;
        name = null;
        tax_code = null;
        city = null;
    }

    public Person(String surname, String name, String tax_code, String city) {
        this.surname = surname;
        this.name = name;
        this.tax_code = tax_code;
        this.city = city;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getTax_code() {
        return tax_code;
    }

    public String getCity() {
        return city;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTax_code(String tax_code) {
        this.tax_code = tax_code;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int bornYear() {
        if (tax_code == null) return -1;
        try {
            String yearCode = tax_code.substring(6, 8);
            Date d = new SimpleDateFormat("yy").parse(yearCode); //Funziona per persone fino ad 80 anni di età
            return Integer.parseInt(new SimpleDateFormat("yyyy").format(d));
        } catch (ParseException | StringIndexOutOfBoundsException e) {
            System.out.println("Tax code is malformed.");
            return -1;
        }
    }

    @Override
    public String toString() {
        return "Surname: " + surname + ", name: " + name + ", tax code: " + tax_code + ", city: " + city;
    }
}