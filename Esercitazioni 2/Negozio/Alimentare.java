import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Alimentare extends Prodotto {
    private Date scadenza;

    public Alimentare(int codiceABarre, String descrizione, double prezzo, Date scadenza) {
        super(codiceABarre, descrizione, prezzo);
        this.scadenza = scadenza;
    }

    public Date getScadenza() {
        return scadenza;
    }

    @Override
    public double applicaSconto() {
        if (scadenza == null) return -1;
        Date today = new Date();
        
        long differenceInMillis = Math.abs(today.getTime() - scadenza.getTime());
        long days = TimeUnit.DAYS.convert(differenceInMillis, TimeUnit.MILLISECONDS);

        if (days < 10) {
            double prezzo = super.getPrezzo() / 100 * 80;
            return prezzo;
        } else {
            return super.getPrezzo();
        }
    }
}