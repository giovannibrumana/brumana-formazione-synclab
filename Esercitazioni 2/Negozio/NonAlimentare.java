public class NonAlimentare extends Prodotto {
    private String materiale;

    public NonAlimentare(int codiceABarre, String descrizione, double prezzo, String materiale) {
        super(codiceABarre, descrizione, prezzo);
        this.materiale = materiale;
    }

    public String getMateriale() {
        return materiale;
    }

    @Override
    public double applicaSconto() {
        if (materiale == null) return -1;
        if (materiale.equalsIgnoreCase("carta") || 
            materiale.equalsIgnoreCase("plastica") || 
            materiale.equalsIgnoreCase("vetro")) {
                double prezzo = super.getPrezzo() / 100 * 90;
                return prezzo;
            } else {
                return super.getPrezzo();
            }
    }
}