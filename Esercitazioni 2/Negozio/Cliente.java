public class Cliente {
    private String cognome;
    private String nome;
    private boolean carta;

    public Cliente(String cognome, String nome, boolean carta) {
        this.cognome = cognome;
        this.nome = nome;
        this.carta = carta;
    }

    public boolean getCarta() {
        return carta;
    }
}