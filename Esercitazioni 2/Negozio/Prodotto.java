public class Prodotto {
    private int codiceABarre;
    private String descrizione;
    private double prezzo;

    public Prodotto(int codiceABarre, String descrizione, double prezzo) {
        this.codiceABarre = codiceABarre;
        this.descrizione = descrizione;
        this.prezzo = prezzo;
    }

    public int getCodiceABarre() {
        return codiceABarre;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public double applicaSconto() {
        return prezzo / 100 * 95;
    }

    @Override
    public String toString() {
        return "ID: " + codiceABarre + ", descrizione: " + descrizione + ", prezzo: " + prezzo;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof Prodotto) {
            Prodotto p = (Prodotto) o;
            if (codiceABarre == p.getCodiceABarre()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + codiceABarre;
        return hash;
    }
}