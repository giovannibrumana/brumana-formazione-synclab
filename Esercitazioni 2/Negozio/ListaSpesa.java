import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ListaSpesa {
    public static void main(String[] args) throws ParseException {
        Cliente lisa = new Cliente("Rossi", "Lisa", true);
        Cliente sergio = new Cliente("Bianchi", "Sergio", false);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Prodotto[] lista = new Prodotto[] {
            new Alimentare(12345, "Fettine di pollo", 5, format.parse("2021-06-12")),
            new Alimentare(23456, "Zucchine", 3, format.parse("2021-06-09")),
            new Alimentare(34567, "Sale grosso", 1, format.parse("2023-01-01")),

            new NonAlimentare(45678, "Terriccio universale", 5, "Terra"),
            new NonAlimentare(56789, "Risma fogli A4", 3, "Carta"),
            new NonAlimentare(67890, "Set contenitori per alimenti", 12, "Plastica")
        };

        System.out.println("Lisa, con cartà fedeltà, ha speso: " + calcolaTotale(lista, lisa));
        System.out.println("Sergio, senza cartà fedeltà, ha speso: " + calcolaTotale(lista, sergio));
    }

    static double calcolaTotale(Prodotto[] lista, Cliente cliente) {
        double totale = 0;
        for (int i = 0; i < lista.length; i++) {
            if (cliente.getCarta() == true) {
                totale += lista[i].applicaSconto();
            } else {
                totale += lista[i].getPrezzo();
            }
        }

        return totale;
    }
}