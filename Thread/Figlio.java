import java.util.Random;

public class Figlio extends Thread {

    @Override
    public void run() {
        Random r = new Random();
        while (true) {
            try {
                synchronized(Cassetto.busta) {
                    if (Cassetto.busta.isEmpty()) {
                        Cassetto.busta.wait();
                    }
                    int soldi = Cassetto.busta.remove(0);
                    System.out.println("Prelevati " + soldi + " Euro.");
                    Cassetto.busta.notify();
                }
                sleep(r.nextInt(1000));
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
