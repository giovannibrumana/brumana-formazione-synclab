import java.util.Random;

public class Genitore extends Thread {

    @Override
    public void run() {
        Random r = new Random();
        for (int i = 0; i < 10; i++) {
            try {
                synchronized(Cassetto.busta) {
                    if (Cassetto.busta.isEmpty() == false) {
                        Cassetto.busta.wait();
                    }

                    int soldi = r.nextInt(20) + 1;
                    Cassetto.busta.add(soldi);
                    System.out.println("Depositati " + soldi + " Euro.");
                    Cassetto.busta.notify();
                }
                sleep(r.nextInt(1000));
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
