import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private CityThread[] clients;
    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }

    public Server() {
        clients = new CityThread[3];
    }

    public void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(2021);
            for (int i = 0; i < clients.length; i++) {
                System.out.println("In attesa della citta' " + (i + 1) + ":");
                Socket socket = serverSocket.accept();
                
                clients[i] = new CityThread(socket);
                clients[i].start();
            }

            for (int i = 0; i < clients.length; i++) {
                for (int j = 0; j < clients.length; j++) {
                    if (clients[i].equals(clients[j])) {
                        continue;
                    }
                    clients[i].sendMessage(clients[j].toString());
                }
            }

            for (int i = 0; i < clients.length; i++) {
                clients[i].sendMessage("bye");
                clients[i].closeSocket();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}