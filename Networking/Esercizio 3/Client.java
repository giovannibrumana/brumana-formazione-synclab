import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client extends Thread {

    private Socket socket;
    private InputStreamReader inputStreamReader;
    private BufferedReader bufferedReader;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            Socket socket = new Socket("localhost", 2021);
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);

            String receiveMessage;
            String sendMessage;
            System.out.println("Inserire i dati della citta' nel formato \"nome_citta, descrizione_meteo, temperatura\":");
            sendMessage = scanner.nextLine();
            writer.println(sendMessage);
            writer.flush();
            
            Client c = new Client(socket);
            c.start();

            System.out.println("Attendi che le altre citta' inviino i loro dati");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public Client(Socket socket) {
        this.socket = socket;
        try {
            inputStreamReader = new InputStreamReader(socket.getInputStream());
            bufferedReader = new BufferedReader(inputStreamReader);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void run() {
        while (true) {
            try {
                String response = bufferedReader.readLine();
                if (response != null) {
                    if (response.equals("bye")) {
                        bufferedReader.close();
                        inputStreamReader.close();
                        socket.close();
                        return;
                    }

                    System.out.println(response);
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
