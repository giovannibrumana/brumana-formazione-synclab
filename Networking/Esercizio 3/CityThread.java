import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class CityThread extends Thread {
    private Socket socket;
    private String cityName;
    private String weather;
    private double degrees;

    private InputStream inputStream;
    private BufferedReader reader;
    private OutputStream outputStream;
    private PrintWriter writer;


    public CityThread(Socket socket) throws IOException {
        this.socket = socket;

        inputStream = socket.getInputStream();
        reader = new BufferedReader(new InputStreamReader(inputStream));
        outputStream = socket.getOutputStream();
        writer = new PrintWriter(outputStream, true);

        String receivedMessage = reader.readLine();
        String[] params = receivedMessage.split(",");
        cityName = params[0].trim();
        weather = params[1].trim();
        degrees = Double.parseDouble(params[2].trim());
    }

    public Socket getSocket() {
        return socket;
    }

    public String getCityName() {
        return cityName;
    }

    public String getWeather() {
        return weather;
    }

    public double getDegrees() {
        return degrees;
    }

    public void run() {
        
    }

    public void sendMessage(String message) {
        writer.println(message);
        writer.flush();
    }

    public void closeSocket() throws IOException {
        reader.close();
        inputStream.close();
        writer.close();
        outputStream.close();
        socket.close();
    }

    @Override
    public String toString() {
        return cityName + ", " + weather + ", " + degrees;
    }

    @Override
    public int hashCode() {
        int n = 7;
        n = n * socket.hashCode();
        return n;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CityThread) {
            CityThread other = (CityThread) o;
            if (socket.equals(other.getSocket())) {
                return true;
            }
        }
        return false;
    }
}