import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class Server {
    public static void main(String[] args) {
        try {
            int counter = 1;
            ServerSocket serverSocket = new ServerSocket(2021);
            while (true) {
                Socket socket = serverSocket.accept();

                InputStream inputStream = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String name = reader.readLine();
                if (name == null || name.isEmpty()) {
                    continue;
                }
                OutputStream outputStream = socket.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
                writer.write("Ciao " + name + "!\n" + (new Date()).toString() + "\n" + "Sei il visitatore numero " + counter++);
                writer.flush();

                reader.close();
                inputStream.close();
                writer.close();
                outputStream.close();
                socket.close();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
