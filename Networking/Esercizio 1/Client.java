import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Inserisci il tuo nome: ");
        String s = scanner.nextLine();
        try {
            Socket socket = new Socket("localhost", 2021);
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println(s);
            InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String response;
            while ((response = bufferedReader.readLine()) != null) {
                System.out.println(response);
            }

            writer.close();
            bufferedReader.close();
            inputStreamReader.close();
            socket.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
