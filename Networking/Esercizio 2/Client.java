import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            Socket socket = new Socket("localhost", 2021);
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String receiveMessage;
            String sendMessage;
            while(true) {
                sendMessage = scanner.nextLine();
                writer.println(sendMessage);
                writer.flush();
                if((receiveMessage = bufferedReader.readLine()) != null) {
                    System.out.println(receiveMessage);
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
