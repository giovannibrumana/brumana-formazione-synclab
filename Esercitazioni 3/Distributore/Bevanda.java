public class Bevanda {
    private String nome;
    private double prezzo;

    public Bevanda(String nome, double prezzo) {
        this.nome = nome;
        this.prezzo = prezzo;
    }

    public String getNome() {
        return nome;
    }

    public double getPrezzo() {
        return prezzo;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof Bevanda == false) return false;
        Bevanda b = (Bevanda) o;
        return b.getNome() == nome && b.getPrezzo() == prezzo;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result = 31 * result + nome.hashCode();
        result = 31 * result + (int) prezzo;
        return result;
    }
}