import java.util.HashMap;

public class Distributore {
    private HashMap<String, Bevanda> tipi;
    private HashMap<Integer, Double> tessere;
    private Colonna[] colonne;

    public Distributore() {
        this.tipi = new HashMap<>();
        this.tessere = new HashMap<>();
        colonne = new Colonna[4];
        for (int i = 0; i < colonne.length; i++) {
            colonne[i] = new Colonna();
        }
    }

    private void scalaCredito(int codiceTessera, double credito) {
        if (tessere.get(codiceTessera) == null) return;
        double oldCredito = tessere.get(codiceTessera);
        tessere.remove(codiceTessera);
        tessere.put(codiceTessera, oldCredito - credito);
    }

    private void aggiungiCredito(int codiceTessera, double credito) {
        scalaCredito(codiceTessera, -credito);
    }

    public void aggiungiBevanda(String codice, String nome, double prezzo) {
        if (tipi.containsKey(codice) == false) {
            tipi.put(codice, new Bevanda(nome, prezzo));
            System.out.println("Bevanda aggiunta (" + nome + ").");
        } else {
            System.out.println("Errore - bevanda già inserita.");
        }
    }

    public double getPrice(String codice) throws BevandaNonValida {
        if (tipi.containsKey(codice) == false) throw new BevandaNonValida();
        return tipi.get(codice).getPrezzo();
    }

    public String getName(String codice) throws BevandaNonValida {
        if (tipi.containsKey(codice) == false) throw new BevandaNonValida();
        return tipi.get(codice).getNome();
    }


    public void aggiungiTessera(int codice) {
        if (tessere.containsKey(codice) == false) {
            tessere.put(codice, 0.0);
            System.out.println("Tessera aggiunta.");
        } else {
            System.out.println("Errore - tessera già inserita.");
        }
    }

    public void caricaTessera(int codice, double credito) throws TesseraNonValida {
        if (tessere.containsKey(codice) == false) throw new TesseraNonValida();
        aggiungiCredito(codice, credito);
        System.out.println("Credito aggiunto (" + credito + ").");
    }

    public double leggiCredito(int codice) throws TesseraNonValida {
        if (tessere.containsKey(codice) == false) throw new TesseraNonValida();
        return tessere.get(codice);
    } 


    public void aggiornaColonna(int indice, String bevanda, int quantità) throws BevandaNonValida, ColonnaNonEsistente {
        if (tipi.get(bevanda) == null) throw new BevandaNonValida();
        if (indice < 1 || indice > 4) throw new ColonnaNonEsistente();
        colonne[indice - 1].setBevanda(tipi.get(bevanda), quantità);
        System.out.println(quantità + " unita' di bevanda aggiunte.");
    }

    public int lattineDisponibili(String codice) throws BevandaNonValida {
        Bevanda bevanda = tipi.get(codice);
        if (bevanda == null) throw new BevandaNonValida();
        int totale = 0;
        for (Colonna c : colonne) {
            if (c.getBevanda() == null) continue;
            if (c.getBevanda().equals(bevanda)) {
                totale += c.getQuantità();
            }
        }
        return totale;
    }


    public double eroga(String codiceBevanda, int codiceTessera)
        throws BevandaNonValida, TesseraNonValida, CreditoInsufficiente, BevandaEsaurita {
        if (tipi.get(codiceBevanda) == null) throw new BevandaNonValida();
        if (tessere.get(codiceTessera) == null) throw new TesseraNonValida();
        if (tessere.get(codiceTessera) - tipi.get(codiceBevanda).getPrezzo() < 0) throw new CreditoInsufficiente();

        Bevanda bevanda = tipi.get(codiceBevanda);
        double credito = tessere.get(codiceTessera);
        for (int i = 0; i < colonne.length; i++) {
            if (colonne[i].getBevanda() == null) continue;
            if (colonne[i].getBevanda().equals(bevanda)) {
                if (colonne[i].getQuantità() > 0) {
                    scalaCredito(codiceTessera, colonne[i].getBevanda().getPrezzo());
                    colonne[i].erogaUno();
                    return i + 1;
                }
            }
        }

        throw new BevandaEsaurita();
    }

}

class BevandaNonValida extends RuntimeException {

}

class TesseraNonValida extends RuntimeException {

}

class CreditoInsufficiente extends RuntimeException {

}

class BevandaEsaurita extends RuntimeException {

}

class ColonnaNonEsistente extends RuntimeException {

}