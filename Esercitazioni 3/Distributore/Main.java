public class Main {
    public static void main(String[] args) {
        success();
        //failure1();
        //failure2();
        //failure3();
        //failure4();
        //failure5();
    }

    static void success() {
        Distributore d = new Distributore();
        d.aggiungiBevanda("A", "Coca cola", 2);
        d.aggiungiBevanda("B", "Fanta", 1.5);
        d.aggiungiBevanda("C", "Sprite", 1.2);
        d.aggiungiBevanda("A", "aaaa", 1);

        d.aggiungiTessera(1);
        d.caricaTessera(1, 10);

        assert d.getName("A").equals("Coca cola");
        assert d.getPrice("B") == 1.5;

        d.aggiornaColonna(1, "A", 10);
        assert d.eroga("A", 1) == 1;

        d.aggiornaColonna(4, "C", 1);
        assert d.eroga("C", 1) == 4;
    }

    static void failure1() {
        Distributore d = new Distributore();
        d.aggiungiBevanda("C", "Sprite", 1);

        d.eroga("A", 1);
    }

    static void failure2() {
        Distributore d = new Distributore();
        d.aggiungiBevanda("A", "Coca cola", 2);

        d.eroga("A", 1);
    }

    static void failure3() {
        Distributore d = new Distributore();
        d.aggiungiBevanda("A", "Coca cola", 2);

        d.aggiornaColonna(10, "A", 1);
    }

    static void failure4() {
        Distributore d = new Distributore();
        d.aggiungiBevanda("A", "Coca cola", 2);
        d.aggiungiTessera(1);

        d.eroga("A", 1);
    }

    static void failure5() {
        Distributore d = new Distributore();
        d.aggiungiBevanda("A", "Coca cola", 2);
        d.aggiungiTessera(1);
        d.caricaTessera(1, 4);

        d.aggiornaColonna(1, "A", 1);
        d.eroga("A", 1);
        d.eroga("A", 1);
    }
}