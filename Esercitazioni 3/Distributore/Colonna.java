public class Colonna {
    private Bevanda bevanda;
    private int quantità;

    public Colonna() {
        bevanda = null;
        quantità = 0;
    }

    public Bevanda getBevanda() {
        return bevanda;
    }

    public int getQuantità() {
        return quantità;
    }

    public void setBevanda(Bevanda bevanda, int quantità) {
        this.bevanda = bevanda;
        this.quantità = quantità;
    }

    public void erogaUno() {
        if (quantità > 0) {
            quantità -= 1;
        }
    }

}