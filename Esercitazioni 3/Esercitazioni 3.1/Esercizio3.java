import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class Esercizio3 {
    public static void main(String[] args) {
        provaEx3();
    }

    static LinkedList<Integer> mergeOrdinato(Iterator<Integer> a, Iterator<Integer> b) {
        Integer first = null;
        Integer second = null;
        LinkedList<Integer> output = new LinkedList<>();
        while (a.hasNext() || b.hasNext()) {
            if (first == null && a.hasNext()) {
                first = a.next();
            }
            if (second == null && b.hasNext()) {
                second = b.next();
            }
            if (first != null && second != null) {
                if (first.compareTo(second) < 0) {
                    output.add(first);
                    first = null;
                } else if (first.compareTo(second) == 0) {
                    output.add(first);
                    first = null;
                    output.add(second);
                    second = null;
                } else {
                    output.add(second);
                    second = null;
                }
            } else {
                if (first == null) {
                    output.add(second);
                    second = null;
                } else {
                    output.add(first);
                    first = null;
                }
            }
        }

        if (first != null) {
            output.add(first);
        }

        if (second != null) {
            output.add(second);
        }
        return output;
    }

    static void provaEx3() {
        LinkedList<Integer> list1 = creaRandomCrescente(5);
        LinkedList<Integer> list2 = creaRandomCrescente(5);
        LinkedList<Integer> list3 = mergeOrdinato(list1.iterator(), list2.iterator());

        stampa(list1.iterator());
        stampa(list2.iterator());
        stampa(list3.iterator());
    }

    static LinkedList<Integer> creaRandomCrescente(int n) {
        Random r = new Random();
        int element = 0;

        LinkedList<Integer> list = new LinkedList<>();
        list.add(element);

        for (int i = 0; i < n; i++) {
            element += r.nextInt(100);
            list.add(element);
        }

        return list;
    }

    static void stampa(Iterator<Integer> i) {
        if(i.hasNext()) System.out.print("<" + i.next() + ">");
        while(i.hasNext()) {
            System.out.print(", <" + i.next() + ">");
        }
        System.out.println();
    }
}
