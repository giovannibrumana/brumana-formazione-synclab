import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Esercizio1 {
    public static void main(String[] args) {
        provaEx1();
    }

    static LinkedList<Integer> creaRandom(int n, int max) {
        LinkedList<Integer> list = new LinkedList<>();
        Random r = new Random();

        for (int i = 0; i < n; i++) {
            list.add(r.nextInt(max));
        }

        return list;
    }

    static LinkedList<Integer> creaRandomSorted(int n, int max) {
        LinkedList<Integer> list = creaRandom(n, max);
        list = (LinkedList<Integer>) sortList(list);

        return list;
    }

    static void stampa(Iterator<Integer> i) {
        if(i.hasNext()) System.out.print("<" + i.next() + ">");
        while(i.hasNext()) {
            System.out.print(", <" + i.next() + ">");
        }
        System.out.println();
    }

    static List<Integer> sortList(List<Integer> list) {
        Integer[] array = Arrays.copyOf(list.toArray(), list.size(), Integer[].class);
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i].compareTo(array[j]) == 1) {
                    Integer tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        }

        if (list instanceof LinkedList<Integer>) {
            LinkedList<Integer> l = new LinkedList<>();
            Collections.addAll(l, array);
            return l;
        } else if (list instanceof ArrayList<Integer>) {
            ArrayList<Integer> l = new ArrayList<>();
            Collections.addAll(l, array);
            return l;
        } else {
            return null;
        }
    }

    static void provaEx1() {
        LinkedList<Integer> linkedList = creaRandom(20, 50);
        stampa(linkedList.iterator());

        ArrayList<Integer> arrayList = creaRandomArr(15, 50);
        stampa(arrayList.iterator());

        LinkedList<Integer> linkedListSorted = creaRandomSorted(20, 50);
        stampa(linkedListSorted.iterator());

        ArrayList<Integer> arrayListSorted = creaRandomArrSorted(15, 50);
        stampa(arrayListSorted.iterator());
    }

    static ArrayList<Integer> creaRandomArr(int n, int max) {
        ArrayList<Integer> list = new ArrayList<>();
        Random r = new Random();

        for (int i = 0; i < n; i++) {
            list.add(r.nextInt(max));
        }

        return list;
    }

    static ArrayList<Integer> creaRandomArrSorted(int n, int max) {
        ArrayList<Integer> list = creaRandomArr(n, max);
        list = (ArrayList<Integer>) sortList(list);        

        return list;
    }

}