import java.util.Iterator;
import java.util.LinkedList;

public class Esercizio4 {
    public static void main(String[] args) {
        provaEx4();
    }
    
    static LinkedList<LinkedList<Integer>> insiemeDiInsiemi(int n) {
        LinkedList<LinkedList<Integer>> output = new LinkedList<>();

        for (int i = 0; i < n; i++) {
            LinkedList<Integer> inner = new LinkedList<>();
            for (int j = 0; j <= i; j++) {
                inner.add(j);
            }
            output.add(inner);
        }

        return output;
    }

    static void stampa(LinkedList<LinkedList<Integer>> list) {
        Iterator<LinkedList<Integer>> outerIterator = list.iterator();

        while (outerIterator.hasNext()) {
            Iterator<Integer> innerIterator = outerIterator.next().iterator();
            stampa(innerIterator);
        }
    }

    static void stampa(Iterator<Integer> i) {
        if(i.hasNext()) System.out.print("<" + i.next() + ">");
        while(i.hasNext()) {
            System.out.print(", <" + i.next() + ">");
        }
        System.out.println();
    }

    static void provaEx4() {
        stampa(insiemeDiInsiemi(10));
    }
}
