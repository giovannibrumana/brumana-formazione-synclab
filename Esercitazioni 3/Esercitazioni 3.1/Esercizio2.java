import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class Esercizio2 {
    public static void main(String[] args) {
        provaEx2();
    }

    static LinkedList<Integer> creaRandomCrescente(int n) {
        Random r = new Random();
        int element = 0;

        LinkedList<Integer> list = new LinkedList<>();
        list.add(element);

        for (int i = 0; i < n; i++) {
            element += r.nextInt(100);
            list.add(element);
        }

        return list;
    }

    static LinkedList<Integer> parseString(LinkedList<String> a) {
        Iterator<String> iterator = a.iterator();
        LinkedList<Integer> list = new LinkedList<>();

        while (iterator.hasNext()) {
            list.add(Integer.parseInt(iterator.next()));
        }

        return list;
    }

    static void provaEx2() {
        LinkedList<Integer> list = creaRandomCrescente(20);
        stampa(list.iterator());

        LinkedList<String> failingList = new LinkedList<>();
        failingList.add("10");
        failingList.add("1");
        failingList.add("0");
        failingList.add("-50");
        failingList.add("123456789123456789123456789"); //Eccezione
        failingList.add(""); //Eccezione
        failingList.add(null); //Eccezione
        failingList.add("40");
        failingList.add("ciao"); //Eccezione
        failingList.add("2893");

        stampa(parseString(failingList).iterator());
    }

    static void stampa(Iterator<Integer> i) {
        if(i.hasNext()) System.out.print("<" + i.next() + ">");
        while(i.hasNext()) {
            System.out.print(", <" + i.next() + ">");
        }
        System.out.println();
    }
}
