import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.DataOutputStream;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String fileName = "prova.txt";

        File file = new File(fileName);

        FileOutputStream outputStream = new FileOutputStream(file);
        DataOutputStream out = new DataOutputStream(outputStream);

        out.writeDouble(5.78);

        out.close();
        outputStream.close();

        FileInputStream inputStream = new FileInputStream(file);
        DataInputStream in = new DataInputStream(inputStream);

        System.out.println(in.readDouble());

        in.close();
        inputStream.close();
    }
}
