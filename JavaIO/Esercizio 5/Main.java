import java.util.Scanner;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        FileInputStream inputStream = new FileInputStream("prova.txt");
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

        String s = "";
        String tmp;
        while ((tmp = in.readLine()) != null) {
            s += tmp;
            s += "\n";
        }

        in.close();
        inputStream.close();

        Scanner scanner = new Scanner(s);
        scanner.useDelimiter("[  \n]");

        int parole = 0;
        while (scanner.hasNext()) {
            scanner.next();
            parole += 1;
        }

        System.out.println("Parole: " + parole);
    }
}
