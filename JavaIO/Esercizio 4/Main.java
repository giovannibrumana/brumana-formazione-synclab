import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        Impiegato i1 = new Impiegato(845583, "Giovanni", "Brumana", "Stagista");
        Impiegato i2 = new Impiegato(123456, "Annalisa", "Marra", "Tutor");

        FileOutputStream outputStream = new FileOutputStream("impiegato.txt");
        ObjectOutputStream out = new ObjectOutputStream(outputStream);
        out.writeObject(i1);
        out.writeObject(i2);

        out.close();
        outputStream.close();

        FileInputStream inputStream = new FileInputStream("impiegato.txt");
        ObjectInputStream in = new ObjectInputStream(inputStream);

        System.out.println((in.readObject()).toString());
        System.out.println((in.readObject()).toString());

        in.close();
        inputStream.close();
    }
}
