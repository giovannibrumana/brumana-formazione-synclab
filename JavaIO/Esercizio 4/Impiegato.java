import java.io.Serializable;

public class Impiegato implements Serializable {
    private int identificativo;
    private String nome;
    private String cognome;
    private String ruolo;

    public Impiegato(int id, String nome, String cognome, String ruolo) {
        identificativo = id;
        this.nome = nome;
        this.cognome = cognome;
        this.ruolo = ruolo;
    }

    public int getIdentificativo() {
        return identificativo;
    }

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }

    public String getRuolo() {
        return ruolo;
    }

    @Override
    public String toString() {
        return identificativo + ", " + nome + ", " + cognome + ", " + ruolo;
    }
}