import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String inputFileName = "prova.txt";

        File inputFile = new File(inputFileName);

        FileInputStream fileInputStream = new FileInputStream(inputFile);
        //DataInputStream inputStream = new DataInputStream(fileInputStream);
        //BufferedInputStream bufferedInputStream = new BufferedInputStream(inputFileStream);
        BufferedReader inputStream = new BufferedReader(new InputStreamReader(fileInputStream));

        String s;
        while ((s = inputStream.readLine()) != null) {
            System.out.println(s);
        }

        inputStream.close();
        fileInputStream.close();
    }
}
