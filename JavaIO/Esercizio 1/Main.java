import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String sourceFileName = "sample.jpg";
        String destFileName = "copy.jpg";

        File sourceFile = new File(sourceFileName);
        File destFile = new File(destFileName);

        FileInputStream inputStream = new FileInputStream(sourceFile);
        FileOutputStream outputStream = new FileOutputStream(destFile);

        outputStream.write(inputStream.readAllBytes());

        inputStream.close();
        outputStream.close();
    }
}
