import java.util.Random;

public class Dado {
    private Random r;

    public Dado() {
        r = new Random();
    }

    public int lancia() {
        return r.nextInt(6) + 1;
    }
}