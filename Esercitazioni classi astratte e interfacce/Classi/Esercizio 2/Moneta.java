import java.util.Random;

public class Moneta {
    Random r;

    public Moneta() {
        r = new Random();
    }

    public String lancia() {
        if (r.nextInt(2) == 0) {
            return "Testa";
        } else {
            return "Croce";
        }
    }
}