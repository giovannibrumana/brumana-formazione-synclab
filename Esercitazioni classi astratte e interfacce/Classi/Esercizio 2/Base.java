import java.util.Random;

public abstract class Base {
    Random r;

    public Base() {
        r = new Random();
    }

    protected int lancia(int bound) {
        return r.nextInt(bound);
    }
}
