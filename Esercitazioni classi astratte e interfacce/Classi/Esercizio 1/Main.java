public class Main {
    public static void main(String[] args) {
        Cilindro c = new Cilindro(4, 10);
        c.stampaVolume();

        Parallelepipedo p = new Parallelepipedo(2, 3, 8);
        p.stampaVolume();
    } 
}