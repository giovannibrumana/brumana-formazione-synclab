public class Cilindro extends Forma {
    public Cilindro(double raggio, double altezza) {
        super(Math.PI * Math.pow(raggio, 2), altezza);
    }

}