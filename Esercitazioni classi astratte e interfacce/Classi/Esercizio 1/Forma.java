public abstract class Forma {
    private double area;
    private double altezza;

    public Forma(double area, double altezza) {
        this.area = area;
        this.altezza = altezza;
    }

    public double calcolaVolume() {
        return area * altezza;
    }

    public void stampaVolume() {
        System.out.println("Volume: " + calcolaVolume() + " cm3");
    }
}