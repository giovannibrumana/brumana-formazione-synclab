public class Main {
    public static void main(String[] args) {
        Add add = new Add(2);
        Sub sub = new Sub(10);
        Mul mul = new Mul(3);
        Div div = new Div(72);

        add.esegui(sub);
        mul.esegui(add);
        div.esegui(mul);
        sub.esegui(div);

        add.stampaValore();
        mul.stampaValore();
        div.stampaValore();
        sub.stampaValore();
    }
}