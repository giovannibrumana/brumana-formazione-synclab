public class Div extends Numero {
    public Div(double valore) {
        super(valore);
    }

    public void esegui(Operazione o) {
        this.setValore(this.getValore() / o.getValore());
    }
}