public class Mul extends Numero {
    public Mul(double valore) {
        super(valore);
    }

    public void esegui(Operazione o) {
        this.setValore(this.getValore() * o.getValore());
    }
}