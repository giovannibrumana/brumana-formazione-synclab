public class Sub extends Numero {
    public Sub(double valore) {
        super(valore);
    }

    public void esegui(Operazione o) {
        this.setValore(this.getValore() - o.getValore());
    }
}