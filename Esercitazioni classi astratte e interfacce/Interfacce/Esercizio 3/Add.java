public class Add extends Numero {
    public Add(double valore) {
        super(valore);
    }

    public void esegui(Operazione o) {
        this.setValore(this.getValore() + o.getValore());
    }
}