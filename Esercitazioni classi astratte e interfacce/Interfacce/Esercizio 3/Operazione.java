public interface Operazione {
    public void esegui(Operazione o);
    public double getValore();
}