public abstract class Numero implements Operazione {
    private double valore;

    public Numero(double valore) {
        this.valore = valore;
    }

    public double getValore() {
        return valore;
    }

    protected void setValore(double nuovoValore) {
        valore = nuovoValore;
    }

    public void stampaValore() {
        System.out.println(valore);
    }
}