public class DistributoreDiBenzina implements Comparable {
    private String città;
    private String proprietario;
    private double capacità;
    private double attuale;

    private final double prezzoBenzina = 1.55;

    public DistributoreDiBenzina(String città, String proprietario, double capacità, double attuale) {
        this.città = città;
        this.proprietario = proprietario;
        this.capacità = capacità;
        this.attuale = attuale;
    }

    public double eroga(double litri) {
        if (litri <= attuale) {
            attuale -= litri;
            return litri * prezzoBenzina;
        } else {
            return 0;
        }
    }

    public int compareTo(Object o) {
        if (o == null) throw new NullPointerException();
        if (o instanceof DistributoreDiBenzina == false) throw new ClassCastException();
        DistributoreDiBenzina d = (DistributoreDiBenzina) o;
        if (this.capacità < d.capacità) {
            return -1;
        } else if (this.capacità > d.capacità) {
            return 1;
        } else {
            return 0;
        }
    }
}