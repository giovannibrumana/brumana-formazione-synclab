public class Main {
    public static void main(String[] args) {
        DistributoreDiBenzina d1 = new DistributoreDiBenzina("Lecco", "Mario Rossi", 100, 25);
        System.out.println(d1.eroga(10));
        System.out.println(d1.eroga(20));

        DistributoreDiBenzina d2 = new DistributoreDiBenzina("Milano", "Giuseppe Verdi", 300, 180);

        System.out.println(d1.compareTo(d2));
        System.out.println(d2.compareTo(d1));
        System.out.println(d1.compareTo(d1));
    }
}