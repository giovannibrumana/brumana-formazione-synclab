public class Quadrato implements ICalcolo {
    private int valore;

    public Quadrato(int n) {
        valore = n * n;
    }

    public void stampaValore() {
        System.out.println(valore);
    }
}