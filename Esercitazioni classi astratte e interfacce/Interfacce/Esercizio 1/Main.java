public class Main {
    public static void main(String[] args) {
        ICalcolo[] a = prog();

        for (ICalcolo o : a) {
            o.stampaValore();
        }
    }

    static ICalcolo[] prog() {
        ICalcolo[] array = new ICalcolo[2];

        array[0] = new Quadrato(5);
        array[1] = new Cubo(5);

        return array;
    }
}