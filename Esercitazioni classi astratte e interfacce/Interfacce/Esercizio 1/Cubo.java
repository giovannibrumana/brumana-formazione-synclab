public class Cubo implements ICalcolo {
    private int valore;

    public Cubo(int n) {
        valore = n * n * n;
    }

    public void stampaValore() {
        System.out.println(valore);
    }
}