import java.util.Random;

public class Esercitazioni1_1 {
    public static void main(String[] args) {
        Random r = new Random();
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = r.nextInt(1000);
        }
        esercizio1(array);

        System.out.println(esercizio2("sasas"));

        esercizio3();

        int[][] matrix = new int[3][4];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = r.nextInt(10);
            }
        }
        esercizio4(matrix);
    }

    static void esercizio1(int[] array) {
        mergesort(array, 0, array.length - 1);

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    static void mergesort(int[] array, int left, int right) {
        if (left < right) {
            int center = left + (right - left) / 2;
            mergesort(array, left, center);
            mergesort(array, center + 1, right);
            merge(array, left, center, right);
        }
    }

    static void merge(int[] array, int left, int center, int right) {
        int i = left;
        int j = center + 1;
        int k = 0;
        int[] b = new int[right - left + 1];

        while (i <= center && j <= right) {
            if (array[i] <= array[j]) {
                b[k] = array[i];
                i += 1;
            } else {
                b[k] = array[j];
                j += 1;
            }
            k += 1;
        }

        while (i <= center) {
            b[k] = array[i];
            i += 1;
            k += 1;
        }

        while (j <= right) {
            b[k] = array[j];
            j += 1;
            k += 1;
        }

        for (k = left; k <= right; k++) {
            array[k] = b[k - left];
        }
    }

    static boolean esercizio2(String s) {
        if (s != null) {
            for (int i = 0; i < s.length() / 2; i++) {
                if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
                    return false;
                }
            }
        }
        return true;
    }

    static void esercizio3() {
        long previous = 0;
        long actual = 1;
        System.out.println(previous);

        for (int i = 0; i < 49; i++) {
            System.out.println(actual);
            long tmp = actual + previous;
            previous = actual;
            actual = tmp;
        }
    }

    static void esercizio4(int[][] matrix) {
        if (matrix == null || matrix.length == 0) return;
        int[][] trasposta = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                trasposta[j][i] = matrix[i][j];
            }
        }

        for (int i = 0; i < trasposta.length; i++) {
            for (int j = 0; j < trasposta[i].length; j++) {
                System.out.print(trasposta[i][j] + " ");
            }
            System.out.println();
        }
    }
}