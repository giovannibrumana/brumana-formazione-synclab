import java.util.Random;

public class EserciziArray {
    public static void main(String[] args) {
        int[] arrayPrimo = {1, 3, 5, 7, 9, 10, 8, 6, 4, 2};
        primo(arrayPrimo);

        Random r = new Random();
        int[] arraySecondo = new int[10];
        for (int i = 0; i < arraySecondo.length; i++) {
            arraySecondo[i] = r.nextInt();
        }
        secondo(arraySecondo);

        int[] arrayTerzo = new int[10];
        for (int i = 0; i < arrayTerzo.length; i++) {
            arrayTerzo[i] = r.nextInt();
        }
        terzo(arrayTerzo);

        String[] s1 = new String[] {"ciao", "mondo", "!", " ", "aaa"};
        String[] s2 = new String[] {"È", "una", "bella", "giornata", "!"};
        quarto(s1, s2);

        return;
    }

    static void primo(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            System.out.println(array[i]);
            System.out.println(array[(array.length - 1) - i]);
        }
    }

    static void secondo(int[] array) {
        int sommaPari = 0;
        int sommaDispari = 0;

        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                sommaPari += array[i];
            } else {
                sommaDispari += array[i];
            }
        }

        if (sommaPari == sommaDispari) {
            System.out.println("Pari e dispari uguali");
        } else {
            System.out.println("Pari e dispari diversi");
        }
    }

    static void terzo(int[] array) {
        int attuale = array[0];
        int contatore = 1;
        for (int i = 1; i < array.length; i++) {
            if (attuale == array[i]) {
                contatore += 1;
            } else {
                attuale = array[i];
                contatore = 1;
            }

            if (contatore == 3) {
                System.out.println("Tre valori consecutivi uguali");
                return;
            }
        }

        System.out.println("NO");
    }

    static void quarto(String[] s1, String[] s2) {
        for (int i = 0; i < s1.length; i++) {
            for (int j = 0; j < s2.length; j++) {
                if (s1[i].equals(s2[j])) {
                    System.out.println("OK");
                    return;
                }
            }
        }

        System.out.println("KO");
    }
}