public class EserciziStringhe {
    public static void main(String[] args) {

        primo("Viva Java");
        
        secondo("Viva Java");
        
        String[] strings = new String[] {"Albero", "foglia", "Radici", "Ramo", "fiore", ""};
        terzo(strings);
        return;
    }

    static void primo(String s) {
        if (s != null) {
            StringBuilder sb = new StringBuilder(s);
            System.out.println(sb.reverse());
        } else {
            System.out.println("Errore");
        }
    }

    static void secondo(String s) {
        if (s != null) {
            for (char c : s.toCharArray()) {
                switch (c) {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                    case 'A':
                    case 'E':
                    case 'I':
                    case 'O':
                    case 'U':
                        System.out.print(c);
                        break;
                    default: break;
                }
            }
        }
        System.out.println();
    }

    static void terzo(String[] strings) {
        boolean finish = false;
        int somma = 0;

        for (String s : strings) {
            if (s == null) continue;
            if (s.equals("")) {
                finish = true;
            }

            if (finish == false) {
                if (s.charAt(0) != s.toLowerCase().charAt(0)) {
                    somma += s.length();
                }
            } else {
                break;
            }
        }

        System.out.println(somma);
    }
}