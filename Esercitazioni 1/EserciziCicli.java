import java.util.Scanner;

public class EserciziCicli {
    public static void main(String[] args) {
        primo(2, 4, 6);

        secondo(5, 8, 9, 12, 7, 6, 1);

        terzo();

        return;
    }

    static void primo(int... sequence) {
        for (int n : sequence) {
            if (n <= 0 || n % 2 == 1) {
                System.out.println("NO");
                return;
            }
        }

        System.out.println("Tutti positivi e pari");
    }

    static void secondo(int... sequence) {
        int somma = 0;
        int contatore = 0;

        for (int n : sequence) {
            if (n % 3 == 0) {
                somma += n;
                contatore += 1;
            }
        }

        System.out.println((double) somma / contatore);
    }

    static void terzo() {
        Scanner s = new Scanner(System.in);
        
        for (int i = 0; i < 5; i++) {
            int n = -1;
            do {
                System.out.print("Quanti caratteri vuoi inserire? ");
                try {
                    n = Integer.parseInt(s.nextLine());
                } catch (NumberFormatException e) {
                    n = -1;
                }
            } while (n < 0);
            
            for (int j = 0; j < n; j++) {
                System.out.println(s.nextLine());
            }
        }

        s.close();
    }
}